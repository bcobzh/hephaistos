#!/usr/bin/env python
import os
import sys
import requests
import logging
import time
from celery import Celery
from celery.utils.log import get_task_logger
from celery.schedules import crontab
from invoke import run

logger = get_task_logger(__name__)
#API_URL= 'http://{API_SERVICE}:{API_PORT}/api'.format(**os.environ)
DB_URL = os.environ['COUCHDB_URL']
NEW_JOB = DB_URL + '/jobs/_design/portail/_view/toQueue'
UPDATE_JOB = DB_URL + '/jobs/{}'
broker = 'amqp://{BROKER_USER}:{BROKER_PASSWD}@{BROKER_URL}//'.format(**os.environ)
#broker = 'amqp://{BROKER_USER}:{BROKER_PASSWD}@{BROKER_URL}//'.format(**os.environ)
backend = 'amqp://{BROKER_USER}:{BROKER_PASSWD}@{BROKER_URL}//'.format(**os.environ)
app = Celery('tasks', broker=broker, backend=backend)

app.conf.update(
    CELERY_TASK_PROTOCOL=1,
)

def update_job(data, logs=''):
    """
    """
    job_id = data['_id']
    del(data['_id'])
    if logs != '':
        data['logs'] = data.get('logs', '') + logs
    update = requests.put(UPDATE_JOB.format(job_id), json=data)
    updated = update.status_code == 201
    if updated:
        data['_rev'] = update.json().get('rev')
    data['_id'] = job_id
    return (updated, data)

@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(15.0, get_new_jobs.s(), name='check every 15s')

@app.task
def get_new_jobs():
    '''
    get jobs to start
    '''
    logger.info('ooooo')
    jobs = requests.get(NEW_JOB)
    for job in jobs.json().get('rows'):
        data = job.get('value')
        data['state'] = 1
        (updated, new_data) = update_job(data)
        if updated:
            run_job.delay(new_data)
        else:
            logger.warn("Celery add job in rabbitmq queue error : "+str(e))



@app.task
def run_job(job):
    '''
    do stuff 
    '''
    print(job)
    logger.warning('in run_job')
    return True

@app.task
def tree4api(site, env, dir_only):
    tree = "tree --inodes --dirsfirst -Ji -I cache -L 10 --noreport"
    path = '~/sisyphe/www/'
    (dirname, basename) = os.path.split(path.rstrip('/'))
    cmd = f'cd {dirname} && {tree} {basename}'
    result = run(cmd, hide=True, warn=True)
    if result.ok:
        return result.stdout
    return []

