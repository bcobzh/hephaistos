# hephaistos

## labo 

### RabbitMQ

```sh 
 podman run -d --pod sisyphe --hostname my-rabbit --name rabbit -p 5672:5672 rabbitmq
 podman run -d --pod sisyphe --hostname my-rabbit --name rabbit -p 5672:5672 rabbitmq
 podman exec -ti rabbit bash

 rabbitmqctl add_vhost hephaistos
 root@my-rabbit:/# rabbitmqctl add_user user secr3t
 rabbitmqctl set_permissions -p hephaistos user ".*" ".*" ".*

```

### celery 
fill the env.sh

```sh
pipenv install
pipenv shell
. env.sh
celery -A tasks worker -B
```

